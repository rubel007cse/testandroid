package co.handymama.test;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.spec.ECField;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*You should almost always run network operations on a thread or as an asynchronous task.
        But if you know better and are willing to accept the consequences,
        and must do network operations on the main thread, you can override the default behavio*/

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String data = getJSON("http://mrubel.com/api/volleyjson/logindata.php", 3000);
       // AuthMsg msg = new Gson().fromJson(data, AuthMsg.class);
        System.out.println("geodata:"+data);

        parse_json("{geodata:"+data+"}");
    }


    public String getJSON(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    static void parse_json(String JSON_DATA){

        try {
            final JSONObject obj = new JSONObject(JSON_DATA);
            final JSONArray geodata = obj.getJSONArray("geodata");
            final int n = geodata.length();
            for (int i = 0; i < n; ++i) {
                final JSONObject person = geodata.getJSONObject(i);
                System.out.println(person.getInt("id"));
                System.out.println(person.getString("name"));
                System.out.println(person.getString("phone"));
                System.out.println(person.getString("email"));
                System.out.println(person.getString("password"));
            }
        } catch (JSONException e ){
            System.out.println(e);
        }

    }
}
