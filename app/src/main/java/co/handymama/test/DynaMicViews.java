package co.handymama.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class DynaMicViews extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dyna_mic_views);

        final CheckBox ct = (CheckBox) findViewById(R.id.ctv);
        ct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ct.isChecked()){
                    ct.setBackgroundResource(R.drawable.select_item);
                } else {
                    ct.setBackgroundResource(R.drawable.disselect_item);
                }
            }
        });


        final LinearLayout lm = (LinearLayout) findViewById(R.id.mylllinear);

        // create the layout params that will be used to define how your
        // button will be displayed
       final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(20,20,20,20);

/*
        //Create four
        for (int j = 0; j <= 8; j++) {
            // Create LinearLayout
            LinearLayout ll = new LinearLayout(this);
            ll.setOrientation(LinearLayout.HORIZONTAL);

            // Create TextView
            TextView product = new TextView(this);
            product.setText(" Product" + j + "    ");
            ll.addView(product);

            // Create TextView
            TextView price = new TextView(this);
            price.setText("  $" + j + "     ");
            ll.addView(price);

            // Create Button
            final Button btn = new Button(this);
            // Give button an ID
            btn.setId(j + 1);
            btn.setText("Add To Cart");
            // set the layoutParams on the button
            btn.setLayoutParams(params);

            final int index = j;
            // Set click listener for button
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Log.i("TAG", "index :" + index);

                    Toast.makeText(getApplicationContext(),
                            "Clicked Button Index :" + index,
                            Toast.LENGTH_LONG).show();

                }
            });

            //Add button to LinearLayout
            ll.addView(btn);
            //Add button to LinearLayout defined in XML
            lm.addView(ll);
        }*/


        // building for cleaning

        final LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        TextView qs = new TextView(this);
        qs.setText("What do you want to be cleaned?");
        ll.addView(qs);


        final CheckBox[] cb = new CheckBox[6];
        final String[] cl_items = {"Floor", "Kitchen", "Toilet", "Furniture/Carpet",
                "Window/Door/Vertical Blind", "Water tank"};
        for(int i = 0; i < 6; i++) {
            cb[i] = new CheckBox(this);
            cb[i].setText(cl_items[i]);
            cb[i].setId(i+6);

            ll.addView(cb[i]);

        }


        //floor koto sqft
        final EditText sqft_ = new EditText(this);
        sqft_.setHint("What is the size of your floor(sq ft)?");
        sqft_.setInputType(InputType.TYPE_CLASS_NUMBER);



            cb[0].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cb[0].isChecked()) {
                        ll.addView(sqft_);
                    } else {
                        ll.removeView(sqft_);
                    }
                }
            });


        final LinearLayout ll_howMany = new LinearLayout(DynaMicViews.this);
        ll_howMany.setOrientation(LinearLayout.HORIZONTAL);
        final CheckBox[] howMany = new CheckBox[6];

        final TextView _howManyToi = new TextView(this);
        _howManyToi.setText("Number of toilets: ");
        _howManyToi.setPadding(10,20,0,20);


        cb[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               /* if(cb[2].isChecked()) {

                    ll.addView(_howManyToi);
                    ll.removeView(ll_howMany);


                    for(int i2 = 0; i2 < 6; i2++) {

                        howMany[i2] = new CheckBox(DynaMicViews.this);
                        howMany[i2].setText(""+(1+i2));
                        howMany[i2].setLayoutParams(params);
                        ll_howMany.addView(howMany[i2]);

                    }

                    ll.addView(ll_howMany);


                } else {
                    ll_howMany.removeView(howMany[0]);
                    ll_howMany.removeView(howMany[1]);
                    ll_howMany.removeView(howMany[2]);
                    ll_howMany.removeView(howMany[3]);
                    ll_howMany.removeView(howMany[4]);
                    ll_howMany.removeView(howMany[5]);
                    ll.removeView(ll_howMany);
                    ll.removeView(_howManyToi);

                }*/
                RadioGroup rgp = new RadioGroup(DynaMicViews.this);
                RadioGroup.LayoutParams rprms;
                ll_howMany.removeAllViews();

               if(cb[2].isChecked()){
                   for(int i=0;i<6;i++){
                       RadioButton radioButton = new RadioButton(DynaMicViews.this);
                       radioButton.setText(""+(i+1));
                       rprms= new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                               ViewGroup.LayoutParams.WRAP_CONTENT);
                       rgp.addView(radioButton, rprms);
                   }
                   ll_howMany.addView(rgp);
                   ll.addView(ll_howMany);
               } else{
                   ll_howMany.removeAllViews();
                   ll.removeView(ll_howMany);
               }


            }
        });



        // Create Button
        final Button btn = new Button(this);
        btn.setText("Submit Order");
        btn.setBackgroundResource(R.drawable.bookahandymamabtn);

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(),
                        "Clicked "+cl_items[0],
                        Toast.LENGTH_LONG).show();

            }
        });

        ll.addView(btn);

        lm.addView(ll);


    }


}